import * as dat from "dat-gui";
import * as React from "react";
import * as ReactDOM from "react-dom";
import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import * as lib from "./lib/userfunc";

import { SimplexNoise } from "three/examples/jsm/math/SimplexNoise.js";
import { GPUComputationRenderer } from "three/examples/jsm/misc/GPUComputationRenderer.js";

// import "../css/style.scss";

import heightmapFS from "./shaders/heightmapFS.frag";
import readWaterLevelFS from "./shaders/readWaterLevelFS.frag";
import fs from "./shaders/shader.frag";
import vs from "./shaders/shader.vert";
import smoothFS from "./shaders/smoothFS.frag";
import waterVS from "./shaders/waterVS.vert";

// let canvas: HTMLCanvasElement;

const randomGen = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min) + min);
};

interface ICanvas3d {
}

class Canvas3d implements ICanvas3d{
  params = {
    fov: 70,
  };
  uniforms = THREE.UniformsUtils.merge([
    THREE.ShaderLib["basic"].uniforms,
    {
      time: {
        type: "f",
        value: 1.0
      },
    }
  ]);  
  material = new THREE.ShaderMaterial({
    vertexShader: vs,
    fragmentShader: fs,
    uniforms : this.uniforms,
    // wireframe: true,
    side: THREE.DoubleSide
  });
  camera = new THREE.PerspectiveCamera();
  scene = new THREE.Scene();
  renderer = new THREE.WebGLRenderer();
  canvasWidth = 0;
  canvasHeight = 0;
  startTime = 0;
  canvasElm;
  mouse = new THREE.Vector2(-1, -1);
  distance = (this.canvasHeight / 2) / Math.tan(lib.degToRad(this.params.fov / 2));
  // gui = new dat.GUI();

  constructor(canvasElm: HTMLCanvasElement, canvasWidth: number, canvasHeight: number){
    this.canvasElm = canvasElm;
    this.canvasWidth = canvasWidth;
    this.canvasHeight = canvasHeight;
    this.startTime = 0;
  }
  
  async init(){
    this.startTime = Date.now();
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvasElm,
      alpha: true,
      antialias: true
    });
    this.renderer.setClearColor( 0x000000, 0);
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(this.canvasWidth, this.canvasHeight );
    
    this.distance = (this.canvasHeight / 2) / Math.tan(lib.degToRad(this.params.fov / 2));
    this.camera = new THREE.PerspectiveCamera(this.params.fov, this.canvasWidth / this.canvasHeight, 1, this.distance * 2 );
    this.camera.aspect = this.canvasWidth / this.canvasHeight;
    this.camera.updateProjectionMatrix();
    this.camera.position.set(0, this.distance / 2, 0);
    this.camera.lookAt( 0, 0, 0 );

    // const controls = new OrbitControls(this.camera, this.canvasElm);
    // controls.enableDamping = true;
    // controls.dampingFactor = 0.25;
    // controls.enableZoom = false;

    this.scene = new THREE.Scene();

    // const axes = new THREE.AxesHelper(250);
    // this.scene.add(axes);

    // this.renderer = new THREE.WebGLRenderer();
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( window.innerWidth, window.innerHeight );

    // this.initGUI();
    // this.renderer.render( this.scene, this.camera );
    // this.animate();
  }

  initGUI(){
    // this.gui = new dat.GUI();
    // const cam = this.gui.addFolder("Camera");
    // cam.add(this.params, "fov", 30, 105).onChange((value: number) => {
    //   this.camera.fov = value;
    //   this.camera.updateProjectionMatrix();
    // });
  }

  imageLoader(src: string){
    return new Promise((resolve: (value: HTMLImageElement) => void, reject: (reason?: any) => void) => {
      const loader = new THREE.ImageLoader();
      loader.load(src, (image) => {
        resolve(image);
      });    
    });
  }
}

class Water extends Canvas3d{
  // Texture width for simulation
  WIDTH = 256;

  // Water size in system units
  BOUNDS = 1024;
  BOUNDS_HALF = this.BOUNDS * 0.5;

  // let container, stats;
  // let camera, scene, renderer;
  mouseMoved = false;
  mouseCoords = new THREE.Vector2();
  raycaster = new THREE.Raycaster();

  waterMesh = new THREE.Mesh();
  meshRay = new THREE.Mesh();
  gpuCompute: any;
  heightmapVariable: any;
  waterUniforms: any;
  smoothShader: any;
  readWaterLevelShader: any;
  readWaterLevelRenderTarget: any;
  readWaterLevelImage: any;
  waterNormal = new THREE.Vector3();

  NUM_SPHERES = 5;
  spheres: Array<any> = [];
  spheresEnabled = true;

  simplex = new SimplexNoise();  

  initWaterGlobal(){
    // set scene parameters
    const sun = new THREE.DirectionalLight( 0xFFFFFF, 1.0 );
    sun.position.set( 300, 400, 175 );
    this.scene.add( sun );

    const sun2 = new THREE.DirectionalLight( 0x40A040, 0.6 );
    sun2.position.set( - 100, 350, - 200 );
    this.scene.add( sun2 );

    // set GUI
    // const effectController = {
    //   mouseSize: 20.0,
    //   viscosity: 0.98,
    //   spheresEnabled: this.spheresEnabled
    // };

    // const valuesChanger = () => {
    //   this.heightmapVariable.material.uniforms[ "mouseSize" ].value = effectController.mouseSize;
    //   this.heightmapVariable.material.uniforms[ "viscosityConstant" ].value = effectController.viscosity;
    //   this.spheresEnabled = effectController.spheresEnabled;
    //   for (let i = 0; i < this.NUM_SPHERES; i++) {
    //     if (this.spheres[i] ) {
    //       this.spheres[i].visible = this.spheresEnabled;
    //     }
    //   }
    // };

    // this.gui.add( effectController, "mouseSize", 1.0, 100.0 ).onChange( valuesChanger );
    // this.gui.add( effectController, "viscosity", 0.9, 0.999 ).onChange( valuesChanger );
    // this.gui.add( effectController, "spheresEnabled", 0, 1 ).onChange( valuesChanger );
    // const buttonSmooth = {
    //   smoothWater: () => {
    //     this.smoothWater();
    //   }
    // };
    // this.gui.add( buttonSmooth, 'smoothWater' );
    this.initWater();

    // this.createSpheres();

    // valuesChanger();
  }

  initWater(){
    const materialColor = 0x112228;
    const geometry = new THREE.PlaneGeometry( this.BOUNDS, this.BOUNDS, this.WIDTH - 1, this.WIDTH - 1 );

    // material: make a THREE.ShaderMaterial clone of THREE.MeshPhongMaterial, with customized vertex shader
    const material: any = new THREE.ShaderMaterial( {
      uniforms: THREE.UniformsUtils.merge( [
        THREE.ShaderLib[ "phong" ].uniforms,
        {
          "heightmap": { value: null }
        }
      ] ),
      vertexShader: waterVS,
      fragmentShader: THREE.ShaderChunk[ "meshphong_frag" ],
      // wireframe: true
    } );

    material.lights = true;

    // Material attributes from THREE.MeshPhongMaterial
    material.color = new THREE.Color( materialColor );
    material.opacity = .25;
    material.specular = new THREE.Color( 0x111111 );
    material.shininess = 50;

    // Sets the uniforms with the material values
    material.uniforms[ "diffuse" ].value = material.color;
    material.uniforms[ "specular" ].value = material.specular;
    material.uniforms[ "shininess" ].value = Math.max( material.shininess, 1e-4 );
    material.uniforms[ "opacity" ].value = material.opacity;

    // Defines
    material.defines.WIDTH = this.WIDTH.toFixed( 1 );
    material.defines.BOUNDS = this.BOUNDS.toFixed( 1 );

    this.waterUniforms = material.uniforms;

    this.waterMesh = new THREE.Mesh( geometry, material );
    this.waterMesh.rotation.x = - Math.PI / 2;
    this.waterMesh.matrixAutoUpdate = false;
    this.waterMesh.updateMatrix();

    this.scene.add( this.waterMesh );

    // THREE.Mesh just for mouse raycasting
    const geometryRay = new THREE.PlaneGeometry( this.BOUNDS, this.BOUNDS, 1, 1 );
    this.meshRay = new THREE.Mesh( geometryRay, new THREE.MeshBasicMaterial( { color: 0xFFFFFF, visible: false } ) );
    this.meshRay.rotation.x = - Math.PI / 2;
    this.meshRay.matrixAutoUpdate = false;
    this.meshRay.updateMatrix();
    this.scene.add( this.meshRay );


    // Creates the gpu computation class and sets it up

    this.gpuCompute = new GPUComputationRenderer( this.WIDTH, this.WIDTH, this.renderer );

    if ( this.isSafari() ) {

      this.gpuCompute.setDataType( THREE.HalfFloatType );

    }

    const heightmap0 = this.gpuCompute.createTexture();

    this.fillTexture( heightmap0 );

    this.heightmapVariable = this.gpuCompute.addVariable( "heightmap", heightmapFS, heightmap0 );

    this.gpuCompute.setVariableDependencies( this.heightmapVariable, [ this.heightmapVariable ] );

    this.heightmapVariable.material.uniforms[ "mousePos" ] = { value: new THREE.Vector2( 10000, 10000 ) };
    this.heightmapVariable.material.uniforms[ "mouseSize" ] = { value: 20.0 };
    this.heightmapVariable.material.uniforms[ "viscosityConstant" ] = { value: 0.98 };
    this.heightmapVariable.material.uniforms[ "heightCompensation" ] = { value: 0 };
    this.heightmapVariable.material.defines.BOUNDS = this.BOUNDS.toFixed( 1 );

    const error = this.gpuCompute.init();
    if ( error !== null ) {
      console.error( error );
    }

    // Create compute shader to smooth the water surface and velocity
    this.smoothShader = this.gpuCompute.createShaderMaterial( smoothFS, { smoothTexture: { value: null } } );

    // Create compute shader to read water level
    this.readWaterLevelShader = this.gpuCompute.createShaderMaterial(readWaterLevelFS, {
      point1: { value: new THREE.Vector2() },
      levelTexture: { value: null }
    } );
    this.readWaterLevelShader.defines.WIDTH = this.WIDTH.toFixed( 1 );
    this.readWaterLevelShader.defines.BOUNDS = this.BOUNDS.toFixed( 1 );

    // Create a 4x1 pixel image and a render target (Uint8, 4 channels, 1 byte per channel) to read water height and orientation
    this.readWaterLevelImage = new Uint8Array( 4 * 1 * 4 );

    this.readWaterLevelRenderTarget = new THREE.WebGLRenderTarget( 4, 1, {
      wrapS: THREE.ClampToEdgeWrapping,
      wrapT: THREE.ClampToEdgeWrapping,
      minFilter: THREE.NearestFilter,
      magFilter: THREE.NearestFilter,
      format: THREE.RGBAFormat,
      type: THREE.UnsignedByteType,
      depthBuffer: false
    });
    this.randomWaterWaving();
  }

  isSafari(){

    return !! navigator.userAgent.match( /Safari/i ) && ! navigator.userAgent.match( /Chrome/i );

  }  

  fillTexture( texture: any ) {

    const waterMaxHeight = 10;

    const noise =( x: number, y: number ) => {
      let multR = waterMaxHeight;
      let mult = 0.025;
      let r = 0;
      for ( let i = 0; i < 15; i ++ ) {
        r += multR * this.simplex.noise( x * mult, y * mult );
        multR *= 0.53 + 0.025 * i;
        mult *= 1.25;
      }
      return r;
    };

    const pixels = texture.image.data;
    let p = 0;
    for ( let j = 0; j < this.WIDTH; j ++ ) {
      for ( let i = 0; i < this.WIDTH; i ++ ) {
        const x = i * 128 / this.WIDTH;
        const y = j * 128 / this.WIDTH;

        pixels[ p + 0 ] = 0;
        // pixels[ p + 0 ] = noise( x, y );
        pixels[ p + 1 ] = pixels[ p + 0 ];
        pixels[ p + 2 ] = 0;
        pixels[ p + 3 ] = 1;

        p += 4;
      }
    }
  }
  
  smoothWater() {
    const currentRenderTarget = this.gpuCompute.getCurrentRenderTarget( this.heightmapVariable );
    const alternateRenderTarget = this.gpuCompute.getAlternateRenderTarget( this.heightmapVariable );
    for ( let i = 0; i < 10; i ++ ) {
      this.smoothShader.uniforms[ "smoothTexture" ].value = currentRenderTarget.texture;
      this.gpuCompute.doRenderTarget( this.smoothShader, alternateRenderTarget );

      this.smoothShader.uniforms[ "smoothTexture" ].value = alternateRenderTarget.texture;
      this.gpuCompute.doRenderTarget( this.smoothShader, currentRenderTarget );
    }
  }

  createSpheres() {
    const sphereTemplate = new THREE.Mesh( new THREE.SphereGeometry( 4, 24, 12 ), new THREE.MeshPhongMaterial( { color: 0xFFFF00 } ) );
    for ( let i = 0; i < this.NUM_SPHERES; i ++ ) {
      let sphere = sphereTemplate;
      if ( i < this.NUM_SPHERES - 1 ) {
        sphere = sphereTemplate.clone();
      }

      sphere.position.x = ( Math.random() - 0.5 ) * this.BOUNDS * 0.7;
      sphere.position.z = ( Math.random() - 0.5 ) * this.BOUNDS * 0.7;

      sphere.userData.velocity = new THREE.Vector3();

      this.scene.add( sphere );
      this.spheres[ i ] = sphere;
    }
  }  

  sphereDynamics() {
    const currentRenderTarget = this.gpuCompute.getCurrentRenderTarget( this.heightmapVariable );
    this.readWaterLevelShader.uniforms[ "levelTexture" ].value = currentRenderTarget.texture;
    for ( let i = 0; i < this.NUM_SPHERES; i ++ ) {
      const sphere = this.spheres[ i ];

      if ( sphere ) {
        // Read water level and orientation
        const u = 0.5 * sphere.position.x / this.BOUNDS_HALF + 0.5;
        const v = 1 - ( 0.5 * sphere.position.z / this.BOUNDS_HALF + 0.5 );
        this.readWaterLevelShader.uniforms[ "point1" ].value.set( u, v );
        this.gpuCompute.doRenderTarget( this.readWaterLevelShader, this.readWaterLevelRenderTarget );

        this.renderer.readRenderTargetPixels( this.readWaterLevelRenderTarget, 0, 0, 4, 1, this.readWaterLevelImage );
        const pixels = new Float32Array( this.readWaterLevelImage.buffer );

        // Get orientation
        this.waterNormal.set( pixels[ 1 ], 0, - pixels[ 2 ] );

        const pos = sphere.position;

        // Set height
        pos.y = pixels[ 0 ];

        // Move sphere
        this.waterNormal.multiplyScalar( 0.1 );
        sphere.userData.velocity.add( this.waterNormal );
        sphere.userData.velocity.multiplyScalar( 0.998 );
        pos.add( sphere.userData.velocity );

        if ( pos.x < - this.BOUNDS_HALF ) {
          pos.x = - this.BOUNDS_HALF + 0.001;
          sphere.userData.velocity.x *= - 0.3;
        } else if ( pos.x > this.BOUNDS_HALF ) {
          pos.x = this.BOUNDS_HALF - 0.001;
          sphere.userData.velocity.x *= - 0.3;
        }
        if ( pos.z < - this.BOUNDS_HALF ) {
          pos.z = - this.BOUNDS_HALF + 0.001;
          sphere.userData.velocity.z *= - 0.3;
        } else if ( pos.z > this.BOUNDS_HALF ) {
          pos.z = this.BOUNDS_HALF - 0.001;
          sphere.userData.velocity.z *= - 0.3;
        }
      }
    }
  }  

  handleResize(){
    const width = window.innerWidth;
    const height = window.innerHeight;

    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(width, height);

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();
    this.render();
  }
  
  setMouseCoords( x: number, y: number ) {
    this.mouseCoords.set( ( x / this.renderer.domElement.clientWidth ) * 2 - 1, - ( y / this.renderer.domElement.clientHeight ) * 2 + 1 );
    this.mouseMoved = true;
  }

  onPointerMove( event: any ) {
    if ( event.isPrimary === false ) return;
    this.setMouseCoords( event.clientX, event.clientY );
  }

  randomWaterWaving(){
    const w = this.canvasWidth, h = this.canvasHeight;
    const randomX = Math.floor(Math.random() * w), randomY = Math.floor(Math.random() * h);
    this.setMouseCoords( randomX, randomY );
    window.setTimeout(this.randomWaterWaving.bind(this), randomGen(1000, 3000));
  }

  frame = 0;

  animate(){
    requestAnimationFrame(() => {
      this.animate();
    });
    this.frame++;
    if(this.frame % 2 === 0) return;
    this.render();
  }

  render(){
    // Set uniforms: mouse interaction
    const uniforms = this.heightmapVariable.material.uniforms;
    if ( this.mouseMoved ) {

      this.raycaster.setFromCamera( this.mouseCoords, this.camera );

      const intersects = this.raycaster.intersectObject( this.meshRay );

      if ( intersects.length > 0 ) {

        const point = intersects[ 0 ].point;
        uniforms[ "mousePos" ].value.set( point.x, point.z );

      } else {

        uniforms[ "mousePos" ].value.set( 10000, 10000 );

      }

      this.mouseMoved = false;

    } else {

      uniforms[ "mousePos" ].value.set( 10000, 10000 );

    }

    // Do the gpu computation
    this.gpuCompute.compute();

    if ( this.spheresEnabled ) {

      this.sphereDynamics();

    }

    // Get compute output in custom uniform
    this.waterUniforms[ "heightmap" ].value = this.gpuCompute.getCurrentRenderTarget( this.heightmapVariable ).texture;

    // Render
    this.renderer.render( this.scene, this.camera );
  }  

}

export const ReactCanvas3d = () =>{
  const canvas: React.MutableRefObject<HTMLCanvasElement|null> = React.useRef(null);

  React.useEffect(() => {
    const canvasDOM = canvas.current;
    if(!canvasDOM) return;
    const canvas3d = new Water(canvasDOM, window.innerWidth, window.innerHeight);
    canvas3d.init().then(() => {
      canvas3d.initWaterGlobal();
      canvas3d.animate();
    });

    window.addEventListener("pointermove", ev => {
      canvas3d.onPointerMove(ev);
    });
    window.addEventListener("click", ev => {
      canvas3d.onPointerMove(ev);
    });

    let timerId: any;
    window.addEventListener("resize", () => {
      if (timerId) return;
      timerId = setTimeout(() => {
        timerId = 0;
        canvas3d.handleResize();
      }, 500);
    });
  }, []);

  return(
    <>
      <canvas id="canvas" width="640" height="480" ref={ canvas } />
    </>
  );
};

const Three = () => {
  return (
    <div className="three">
      <ReactCanvas3d />
    </div>
  );
};

export default Three;