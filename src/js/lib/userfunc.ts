export const degToRad = (d: number) => {
  return d * (Math.PI / 180);
};

export const radToDeg = (r: number) =>  {
  return r * (180 / Math.PI);
};

export const randomGen = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min) + min);
};

export const easeInOutCubic = (t: number, b: number, c: number, d: number) => {
	t /= d/2;
	if (t < 1) return c/2*t*t*t + b;
	t -= 2;
	return c/2*(t*t*t + 2) + b;
};

