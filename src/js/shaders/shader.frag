precision mediump float;

uniform sampler2D map1;
uniform sampler2D map2;
uniform sampler2D uDisp;

varying vec3 vColor;
varying vec2 vUv;

void main() {
  vec2 uv = vUv;
  // vec4 disp = texture2D(uDisp, uv);

  // vec2 distortedPosition = vec2(uv.x, uv.y + ticker * disp.r);
  // vec2 distortedPosition2 = vec2(uv.x, uv.y - (1.0 - ticker) * disp.r);

  // vec4 _tex1 = texture2D(map1, distortedPosition);
  // vec4 _tex2 = texture2D(map2, distortedPosition2);

  // vec4 finalTexture = mix(_tex1, _tex2, ticker);

  // vec4 color = vec4(1.0, 1.0, 0.0, 1.0);
  vec4 color = texture2D(map1, uv);
  // vec4 color = vec4(vColor, 1.0);
  gl_FragColor = color;
}