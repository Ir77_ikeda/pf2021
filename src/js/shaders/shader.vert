uniform float index;
uniform float time;

attribute float pindex;
attribute vec3 offset;
attribute float angle;
attribute vec3 colors;

varying vec3 vColor;
varying vec2 vUv;

void main() {
  vec3 pos = position;
  // float amp = 0.5 * sin(time * 0.05 + position.x * 32.0 - index * 10.0);  
  vUv = uv;
  vColor = colors / 255.0;

  pos += offset;
	vec4 mvPosition = modelViewMatrix * vec4(pos, 1.0);
	vec4 finalPosition = projectionMatrix * mvPosition;  
  gl_Position = finalPosition;
}