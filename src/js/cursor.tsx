import * as React from "react";
import * as ReactDOM from "react-dom";
import {StateContext} from "./app";

interface ICursorState{
  mouseX: number;
  mouseY: number;
  status: string | null;
}

// export class Cursor extends React.Component<{}, ICursorState>{
//   constructor(props?: any){
//     super(props);
//     this.state = {
//       mouseX: 0,
//       mouseY: 0,
//       status: null
//     }
//   }

//   componentDidMount(){
//     window.addEventListener("mousemove", ev => {
// //      console.log(ev.clientX, ev.clientY)
//       this.setState({
//         mouseX: ev.clientX,
//         mouseY: ev.clientY
//       })
//     });
//   }

//   setDefault() {
//     this.setState({
//       status: ""
//     })
//     console.log("Default")
//   }
  
//   setHover() {
//     this.setState({
//       status: "hover"
//     })
//     console.log("Hover")
//   }  

//   render(){
//     return(
//       <div
//         className={this.state.status !== null ? `cursor ${this.state.status}` : "cursor"}
//         style={{
//           transform: `translateX(${this.state.mouseX}px) translateY(${this.state.mouseY}px)`,
//         }}
//       ></div>
//     )
//   }
// }

export const Cursor = () => {
  const [mouseX, setMouseX] = React.useState(0);
  const [mouseY, setMouseY] = React.useState(0);
  const {state, dispatch} = React.useContext(StateContext);

  React.useEffect(() => {
    window.addEventListener("mousemove", ev => {
      setMouseX(ev.clientX);
      setMouseY(ev.clientY);
    });
  }, []);

  return (
    <>
      {(!navigator.userAgent.match(/iPhone|iPod|iPad|Android.*Mobile/)) ?
        <div
        className={state.mouseState ? `cursor ${state.mouseState}` : "cursor"}
        style={{
          transform: `translateX(${mouseX}px) translateY(${mouseY}px)`,
        }}>
          <span className="arrow"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-up" className="svg-inline--fa fa-chevron-up fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z"/></svg></span>
        </div>
      :
        null
      }
    </>
  );
};

export default Cursor;