import * as React from "react";
import * as ReactDOM from "react-dom";

import gsap from "gsap";
import {
  useHistory,
  useLocation,
  BrowserRouter,
  Link,
  Redirect,
  Route,
  Switch
} from "react-router-dom";

import "../css/style.scss";
import Cursor from "./cursor";
import * as lib from "./lib/userfunc";
import loaderProgress from "./loader";
import { ReactCanvas3d } from "./three";

import err_404Component from "./components/404";
import aboutComponent from "./components/about";
import indexComponent from "./components/index";

// products
import products_mini_liplayComponent from "./components/products/fujifilm_mini_liplay";
import products_sq1Component from "./components/products/fujifilm_sq1";
import products_nasComponent from "./components/products/nas";
import products_randonneeComponent from "./components/products/randonnee";
import products_sayuriComponent from "./components/products/sayuri";
import products_tsujiComponent from "./components/products/tsuji";

// React state hooks
interface State {
  mouseState: string;
  wrapperClassState: string;
  isViewComplete: boolean;
}

interface Action {
  type: string;
  // type: ActionType,
  payload?: any;
}

export enum ActionType {
  MOUSESTATECHANGE = "MOUSESTATECHANGE",
  WRAPPERCLASSCHANGE = "WRAPPERCLASSCHANGE",
  ISVIEWCOMPLETE = "ISVIEWCOMPLETE",
  SETDEFAULT = "SETDEFAULT"
}

export const StateContext = React.createContext({} as {
  state: State
  dispatch: React.Dispatch<Action>
});

export const ProductContext = React.createContext({} as {
  productId: number,
  setProductId: React.Dispatch<React.SetStateAction<number>>,
  productIdRef: React.MutableRefObject<number>
});

// handle mouse event
export class HandleLink {
  history: any;
  location: any;
  state: any;
  dispatch: any;

  constructor(){
    this.history = useHistory();
    this.location = useLocation();
    this.state = React.useContext(StateContext).state;
    this.dispatch = React.useContext(StateContext).dispatch;
  }

  _hamburgerBtnContext = React.useContext(hamburgerBtnContext);

  handleClickLink = (ev?: any) => {
    const path = ev.currentTarget.dataset.path;
    const reg = /^(http|https):\/\/[\w/:%#\$&\?\(\)~\.=\+\-]+/g;
    if(path.match(reg)) {
      window.open(path, "_blank");
      return;
    }
    this.dispatch({
      type: "MOUSESTATECHANGE",
      payload: "click"
    });
    this.dispatch({
      type: "WRAPPERCLASSCHANGE",
      payload: "blur"
    });    
    this._hamburgerBtnContext.setLinkListState("");
    window.setTimeout(() => {
      this.history.push(path);
      this.dispatch({
        type: "INITWRAPPERCLASS"
      });
    }, 600);
  }
  handleClickBack = () => {
    this.dispatch({
      type: "MOUSESTATECHANGE",
      payload: "click"
    });
    this.dispatch({
      type: "WRAPPERCLASSCHANGE",
      payload: "blur"
    });    
    window.setTimeout(() => {
      this.history.back();
      this.dispatch({
        type: "INITWRAPPERCLASS"
      });
    }, 600);
  }
  handleMouseOver = () => {
    this.dispatch({
      type: "MOUSESTATECHANGE",
      payload: "hover"
    });
  }
  handleMouseOut = () => {
    this.dispatch({
      type: "INITMOUSESTATE"
    });
  }  
}

// setting of children components
export const siteSettings = {
  siteName: "Ikeda Takumi Portfolio",
  siteUrl: "https://www.aukmit.com/"
};

let recentpath:string[] = [];
export const ComponentsGlobalFunc = () => {
  const history = useHistory();
  const location = useLocation();
  const dispatch = React.useContext(StateContext).dispatch;

  const disableHistoryBack = () => {
    history.listen((newLocation, action) => {
      const cond = 
        newLocation.pathname !== location.pathname ||
        newLocation.search !== location.search;

      if (action === "PUSH") {
        if (cond) {
          if(newLocation.pathname !== recentpath[recentpath.length - 1]){
            recentpath.push(newLocation.pathname);
          }
          // console.log(recentpath)
          // Save new location
          location.pathname = newLocation.pathname;
          location.search = newLocation.search;

          // Clone location object and push it to history
          history.push({
            pathname: newLocation.pathname,
            search: newLocation.search
          });
        }        
      } else if(action === "POP"){        
        const _recentpath = recentpath;
        // history.go(1);
        dispatch({
          type: "WRAPPERCLASSCHANGE",
          payload: "blur"
        });    
        window.setTimeout(() => {
          dispatch({
            type: "INITWRAPPERCLASS"
          });
          // history.push(_recentpath);
          _recentpath.pop();
          // console.log(_recentpath)
          history.push(_recentpath[_recentpath.length - 1]);
        }, 600);
      } else {
        history.go(1);
      }
    });    
  };

  React.useEffect(() => {
    if(recentpath.length < 1){
      recentpath.push(location.pathname);
    }
    // window.addEventListener("popstate", ev => {
    //   console.log(ev)
    // })
    disableHistoryBack();
  }, []);

  React.useEffect(() => {
  }, [location]);

  return(
    <></>
  );
};

const hamburgerBtnContext = React.createContext({} as {
  linkListState: string,
  setLinkListState: React.Dispatch<React.SetStateAction<string>>
});

const Header = () => {
  // const history = useHistory();
  const location = useLocation();
  const handleLink = new HandleLink();
  const [headerClassState, setHeaderClassState] = React.useState("");

  const _hamburgerBtnContext = React.useContext(hamburgerBtnContext);
  const toggleLinkListState = (ev: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    ev.preventDefault();
    if(_hamburgerBtnContext.linkListState === ""){
      _hamburgerBtnContext.setLinkListState("active");
    }else{
      _hamburgerBtnContext.setLinkListState("");
    }
  };

  const
    backBtnRef: React.MutableRefObject<HTMLSpanElement | null> = React.useRef(null),
    navBtnRef: React.MutableRefObject<HTMLDivElement | null> = React.useRef(null);

  const changeTextColor = () => {
    const scrollTop = window.pageYOffset;
    const darkBg = document.querySelectorAll(".darkBg");
    if(darkBg !== undefined){
      for(let i = 0, len = darkBg.length; i < len; i++){
        const
          rect = darkBg[i].getBoundingClientRect(),
          top = rect.top + scrollTop,
          height = darkBg[i].clientHeight,
          bottom = top + height;
          // console.log(rect, top, height, bottom);
        
        // backbtn
        const backBtnDom = document.querySelector(".back");
        if(backBtnDom){
          const backBtnDomtop = backBtnDom.getBoundingClientRect().top + scrollTop;
          if(top < backBtnDomtop && backBtnDomtop < bottom){
            backBtnRef.current?.classList.add("white");
            navBtnRef.current?.classList.add("white");
          }else{
            backBtnRef.current?.classList.remove("white");
            navBtnRef.current?.classList.remove("white");
          }
        }
      }
    }else{

    }
  };

  // React.useEffect(() => {
  //   window.addEventListener("scroll", () => {
  //     changeTextColor();
  //   });
  // }, [])

  // React.useEffect(() => {
  //   changeTextColor();
  // }, [location])

  return(
    <header>
      {/* <h1 className="pagetitle" onClick={handleLink.handleClickLink} onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut} data-path="/" >Ikeda Takumi</h1> */}
      {(location.pathname !== "/") ? <span className={`back ${headerClassState}`} onClick={handleLink.handleClickLink} onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut} data-path="/" ref={backBtnRef}><span>Back</span></span> : null }
      <nav className={`${_hamburgerBtnContext.linkListState}`}>
        <div className="hamburger" onClick={toggleLinkListState} onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut} ref={navBtnRef}/>
        <ul className="link_list showpc">
          <li onClick={handleLink.handleClickLink} onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut} data-path="/">
            <span>HOME</span>
          </li>
          <li onClick={handleLink.handleClickLink} onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut} data-path="/about">
            <span>ABOUT</span>
          </li>
        </ul>       
      </nav>
    </header>
  );
};

const Footer = () => {
  const handleLink = new HandleLink();
  
  return(
    <footer>
      <span>&copy; Ikeda Takumi</span>
      <a href="https://gitlab.com/Ir77_ikeda/pf2021" target="_blank" className="code" onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut}>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 586 559">
        <g id="logo_art">
          <path className="st12" d="M461.514,298.355l-18.049-55.587c0.008,0.025,0.011,0.051,0.019,0.076c-0.009-0.029-0.014-0.058-0.022-0.087
            c-0.001-0.002-0.001-0.003-0.001-0.005c0-0.001,0-0.002,0-0.002l-35.83-110.31c-1.96-5.811-7.353-9.711-13.536-9.663
            c-6.201,0.032-11.446,3.857-13.364,9.748L346.721,237.23H239.408l-34.074-104.712c-1.916-5.886-7.162-9.71-13.362-9.742
            c-0.025,0-0.049,0-0.075,0c-6.105,0-11.509,3.876-13.49,9.752l-35.732,110.211l-0.005,0.014c0,0.001,0,0.002,0,0.003
            c-0.009,0.028-0.013,0.056-0.022,0.084c0.008-0.025,0.011-0.051,0.019-0.076l-18.115,55.591c-2.725,8.392,0.232,17.512,7.36,22.697
            L288.328,434.7c0.023,0.017,0.049,0.027,0.072,0.044c0.067,0.048,0.132,0.097,0.2,0.142c-0.064-0.043-0.124-0.09-0.187-0.134
            c0,0,0-0.001-0.001-0.001c0.01,0.008,0.022,0.013,0.033,0.02c0.009,0.006,0.018,0.01,0.027,0.016
            c0.001,0.001,0.002,0.002,0.004,0.003c0.242,0.168,0.493,0.322,0.753,0.463c0.036,0.02,0.068,0.045,0.104,0.064
            c0.001,0,0.001,0.001,0.002,0.001c0.022,0.011,0.042,0.025,0.064,0.036c0.017,0.008,0.035,0.013,0.051,0.021
            c0.012,0.006,0.025,0.01,0.037,0.015c0.029,0.014,0.061,0.023,0.09,0.038c0.136,0.065,0.279,0.118,0.419,0.175
            c0.131,0.054,0.258,0.117,0.392,0.164c0.006,0.002,0.011,0.005,0.017,0.007c0.022,0.008,0.042,0.019,0.065,0.027
            c0.028,0.01,0.055,0.021,0.083,0.03c0.011,0.003,0.022,0.005,0.033,0.008c0.035,0.011,0.073,0.016,0.108,0.026
            c0.013,0.004,0.028,0.006,0.042,0.01c0.188,0.057,0.383,0.098,0.577,0.141c0.076,0.017,0.149,0.041,0.226,0.055
            c0.011,0.002,0.021,0.006,0.033,0.008c0.025,0.005,0.048,0.014,0.074,0.018c0.041,0.007,0.081,0.02,0.123,0.026
            c0.033,0.005,0.067,0.003,0.1,0.008c0.006,0.001,0.011,0,0.017,0.001c0.002,0,0.003,0,0.005,0c0.369,0.053,0.743,0.09,1.124,0.09
            c0.002,0,0.004,0,0.007,0l0,0c0.001,0,0.002,0,0.002,0c0,0,0.001,0,0.001,0c0.001,0,0.002,0,0.003,0
            c0.382,0,0.756-0.037,1.126-0.09c0.001,0,0.003,0,0.004,0c0.006-0.001,0.012,0,0.018-0.001c0.033-0.005,0.068-0.003,0.101-0.008
            c0.042-0.007,0.082-0.019,0.124-0.026c0.025-0.004,0.048-0.013,0.073-0.018c0.011-0.002,0.021-0.006,0.032-0.008
            c0.078-0.015,0.153-0.039,0.231-0.056c0.191-0.042,0.383-0.083,0.57-0.139c0.013-0.004,0.026-0.005,0.039-0.009
            c0.037-0.011,0.075-0.016,0.112-0.027c0.011-0.004,0.023-0.005,0.034-0.008c0.029-0.009,0.057-0.021,0.085-0.031
            c0.022-0.008,0.042-0.019,0.064-0.027c0.006-0.002,0.011-0.005,0.017-0.007c0.142-0.05,0.276-0.116,0.415-0.173
            c0.129-0.054,0.261-0.102,0.387-0.162c0.031-0.015,0.064-0.024,0.094-0.039c0.012-0.006,0.026-0.01,0.038-0.016
            c0.017-0.008,0.035-0.013,0.052-0.022c0.023-0.012,0.045-0.026,0.067-0.037c0,0,0.001,0,0.001-0.001
            c0.037-0.019,0.07-0.046,0.107-0.066c0.258-0.14,0.508-0.293,0.749-0.46c0.019-0.013,0.041-0.023,0.061-0.037
            c0.005-0.004,0.011-0.006,0.016-0.01c0.023-0.017,0.05-0.028,0.073-0.045l156.44-113.65
            C461.282,315.867,464.239,306.747,461.514,298.355z M394.194,142.774l30.68,94.456h-61.36L394.194,142.774z M419.501,253.202
            l-12.519,16.041L314.648,387.55l43.677-134.348H419.501z M285.428,430.707C285.428,430.707,285.428,430.707,285.428,430.707
            c0.008,0.024,0.021,0.046,0.029,0.071C285.449,430.753,285.436,430.731,285.428,430.707z M271.42,387.558L166.624,253.202l0,0
            h61.18L271.42,387.558z M191.875,142.773l30.737,94.457h-61.36L191.875,142.773z M141.304,308.133
            c-1.516-1.103-2.144-3.05-1.563-4.838l13.466-41.325l98.67,126.502L141.304,308.133z M288.053,434.489
            c-0.031-0.025-0.061-0.052-0.091-0.078c-0.006-0.005-0.012-0.012-0.019-0.017c-0.06-0.05-0.119-0.101-0.177-0.153
            c-0.114-0.099-0.226-0.2-0.333-0.306c0.009,0.008,0.019,0.015,0.028,0.023c0.012,0.011,0.025,0.02,0.037,0.031
            c0.229,0.219,0.47,0.425,0.722,0.615c0.003,0.002,0.005,0.005,0.008,0.007c0.012,0.009,0.022,0.02,0.034,0.03
            C288.193,434.591,288.121,434.543,288.053,434.489z M293.028,402.392l-25.665-79.059l-22.766-70.131h96.933L293.028,402.392z
            M298.281,434.241c-0.06,0.052-0.118,0.104-0.179,0.154c-0.007,0.006-0.014,0.013-0.021,0.019c-0.031,0.025-0.06,0.052-0.09,0.077
            c-0.066,0.053-0.138,0.101-0.207,0.152c0.012-0.009,0.022-0.021,0.035-0.029c0.002-0.002,0.004-0.004,0.006-0.006
            c0.252-0.19,0.492-0.394,0.719-0.613c0.009-0.009,0.02-0.016,0.029-0.024c0.012-0.011,0.025-0.02,0.036-0.031
            C298.503,434.043,298.392,434.143,298.281,434.241z M444.766,308.13l-110.557,80.317l98.703-126.467l13.412,41.307
            C446.906,305.083,446.279,307.03,444.766,308.13z"/>
        </g>
        </svg>
      </a>
    </footer>
  );
};


const FirstAccessView = () => {
  const [overlayClassState, setOverlayClassState] = React.useState("");
  const nameRef = React.useRef(null);
  const state = React.useContext(StateContext).state;
  const dispatch = React.useContext(StateContext).dispatch;

  let loadedCount = 0;
  const [per, setPer] = React.useState(0);

  const imageList = [
    "../assets/proj/mini_liplay/1.png",
    "../assets/proj/mini_link/1.png",
    "../assets/proj/sq1/1.png",
    "../assets/proj/nas/1.png",
    "../assets/proj/randonnee/1.png",
    "../assets/proj/sayuri/1.png",
    "../assets/proj/tsujimurashirou/1.png"
  ];
  
  const loaderProgress = () => {
    const loadImagePromise = (src: string) => {
      return new Promise((resolve, reject) => {
        const image = new Image();
        image.onload = () => {
          loadedCount++;
          setPer(Math.floor((loadedCount / imageList.length) * 100));
          resolve(image);
        };
        image.onerror = error => { reject(error); };
        image.src = src;
      });
    };
  
    let promiseList: any = [];
    imageList.forEach(image => {
      promiseList.push(loadImagePromise(image));
    });
  
    Promise.all(promiseList).then(() => {
      window.setTimeout(() => {
        sessionStorage.setItem("isFirstAccess", "false");
        setOverlayClassState("hide");
        dispatch({
          type: "ISVIEWCOMPLETE"
        });    
      }, 1000);
      // window.addEventListener("click", () => {
      //   setOverlayClassState("hide");
      // });    
    });
  };

  React.useEffect(() => {
    loaderProgress();
    const isFirst = sessionStorage.getItem("isFirstAccess");
    if(isFirst && isFirst === "false") {
      window.setTimeout(() => {
        setOverlayClassState("hide");
        dispatch({
          type: "ISVIEWCOMPLETE"
        });    
      }, 2000);
      return;
    }
  }, []);

  return(
    <div className={`overlay_first ${overlayClassState}`}>
      <span className="name upper" ref={nameRef} style={{
        clipPath: `polygon(0% 100%, 0% ${100 - per}%, 100% ${100 - per}%, 100% 100%)`
      }}>Ikeda Takumi</span>
      <span className="name lower" ref={nameRef}>Ikeda Takumi</span>
    </div>
  );
};

const App = () => {
  const [mouseState, setMouseState] = React.useState("");
  const [wrapperClassState, setWrapperClassState] = React.useState("");
  const [isViewComplete, setIsViewComplete] = React.useState("");
  const [linkListState, setLinkListState] = React.useState("");
  const isFirst = sessionStorage.getItem("isFirstAccess");

  const toggleLinkListState = () => {
    if(linkListState === ""){
      setLinkListState("active");
    }else{
      setLinkListState("");
    }
  };

  const reducer = (state: State, action: Action) => {
    // console.log(state, action)
    switch(action.type) {
      case "ISVIEWCOMPLETE":
        return {
          ...state,
          isViewComplete: true
        };
      case "MOUSESTATECHANGE":
        return {
          ...state,
          mouseState: action.payload
        };
      case "WRAPPERCLASSCHANGE":
        return {
          ...state,
          wrapperClassState: action.payload
        };  
      case "INITMOUSESTATE":
        return {
          ...state,
          mouseState: "",
        };
      case "INITWRAPPERCLASS":
        return {
          ...state,
          wrapperClassState: ""
        };  
      case "SETDEFAULT":
        return {
          ...state,
          mouseState: "",
          wrapperClassState: "",
          isViewComplete: false
        };
      default:
        return state;
    }
  };
  
  const initialState = {
    mouseState: "",
    wrapperClassState: "",
    isViewComplete: false,
  };

  const [state, dispatch] = React.useReducer(reducer, initialState);

  React.useEffect(() => {
    const isFirst = sessionStorage.getItem("isFirstAccess");
  }, []);

  const [productId, setProductId] = React.useState(0);
  const productIdRef = React.useRef(0);
  productIdRef.current = productId;


  return (
    <StateContext.Provider value={{ state, dispatch }}>
      <hamburgerBtnContext.Provider value={{ linkListState, setLinkListState }}>
        <ProductContext.Provider value={{productId, setProductId, productIdRef}}>
          {/* <div className="bgbtn" onClick={handleBtnClick}> TEST </div> */}
          <div className={`wrapper ${state.wrapperClassState}`}>
            <FirstAccessView /> 
            {/* {(!sessionStorage.getItem("isFirstAccess") || sessionStorage.getItem("isFirstAccess") === "false") ?
              <FirstAccessView /> 
              :
              <></>
            } */}
            {/* <div className="testcanvas" id="canvas"/> */}
            <ReactCanvas3d />
            <Cursor />
            <div className="contents">
              <BrowserRouter>
                <ComponentsGlobalFunc />
                <Header />
                <Switch>     
                  <Route exact path="/" component={indexComponent} />
                  <Route path="/about" component={aboutComponent} />
                  <Route path="/mini_liplay" component={products_mini_liplayComponent} />
                  <Route path="/sq1" component={products_sq1Component} />
                  <Route path="/nas" component={products_nasComponent} />
                  <Route path="/randonnee" component={products_randonneeComponent} />
                  <Route path="/tsujimurashirou" component={products_tsujiComponent} />
                  <Route path="/sayuri" component={products_sayuriComponent} />
                  <Route component={err_404Component} />
                </Switch>
                <Footer />
              </BrowserRouter>      
            </div>
          </div>
        </ProductContext.Provider>
      </hamburgerBtnContext.Provider>
    </StateContext.Provider>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
export default App;