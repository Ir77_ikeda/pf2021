const imageList = [
  "../assets/proj/mini_liplay/1.png",
  "../assets/proj/mini_link/1.png",
  "../assets/proj/sq1/1.png",
  "../assets/proj/nas/1.png",
  "../assets/proj/randonnee/1.png",
  "../assets/proj/sayuri/1.png",
  "../assets/proj/tsujimurashirou/1.png"
];

const loaderProgress = () => {
  let loadedCount = 0;
  const loadImagePromise = (src: string) => {
    return new Promise((resolve, reject) => {
      const image = new Image();
      image.onload = () => {
        loadedCount++;
        resolve(image);
      };
      image.onerror = error => { reject(error); };
      image.src = src;
    });
  };

  let promiseList: any = [];
  imageList.forEach(image => {
    promiseList.push(loadImagePromise(image));
  });

  Promise.all(promiseList).then(() => {

  });
};


export default loaderProgress;