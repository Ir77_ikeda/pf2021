import * as React from "react";
import {HandleLink, ProductContext, StateContext} from "../app";
// import gsap from "gsap";

// t：アニメーションの経過時間」「b：始点」「c：変化量」「d：変化にかける時間」
export const easeInOutCubic = (t: number, b: number, c: number, d: number) => {
	t /= d/2;
	if (t < 1) return c/2*t*t*t + b;
	t -= 2;
	return c/2*(t*t*t + 2) + b;
};

class SwipeEventListener{
  startX = 0;
  startY = 0;
  moveX = 0;
  moveY = 0;
  dist = 100;

  touchStart(ev: TouchEvent) {
    // ev.preventDefault();
    this.startX = ev.touches[0].pageX;
    this.startY = ev.touches[0].pageY;
    this.moveX = ev.touches[0].pageX;
    this.moveY = ev.touches[0].pageY;
  }

  touchMove(ev: TouchEvent) {
    // ev.preventDefault();
    this.moveX = ev.changedTouches[0].pageX;
    this.moveY = ev.changedTouches[0].pageY;
  }

  touchEnd(ev: TouchEvent) {
    // ev.preventDefault();
    if (this.startX > this.moveX && this.startX > this.moveX + this.dist) {
      return "left";
    }
    else if (this.startX < this.moveX && this.startX + this.dist < this.moveX) {
      return "right";
    }
  }
}

export const Scroll_guide = () =>{
  const {state, dispatch} = React.useContext(StateContext);
  const [classState, setClassState] = React.useState("hidden");

  React.useEffect(() => {
    const isFirst = sessionStorage.getItem("isFirstAccess");
    if(!isFirst || isFirst !== "false"){
      setClassState("");
    }
    if(state.isViewComplete === true){
      window.setTimeout(() => {
        setClassState("hidden");
      }, 2500);
    }
  }, [state.isViewComplete]);

  return(
    <>
    {/* {(classState !== "hidden") ?  */}
      <div className={`scrollGuide_outer ${classState}`}>
        <div className="scrollGuide">
          <span className="txt">Scroll</span>
          <div className="icon" />
        </div>
      </div>
      {/* :
      <></>
    } */}
    </>
  );
};


const Product = () => {
  const handleLink = new HandleLink();

  const lists = [
    {
      id: 0,
      name: "instax mini Liplay",
      thumb: "./assets/proj/mini_liplay/1.png",
      link: "./mini_liplay"
    },
    {
      id: 1,
      name: "instax SQUARE SQ1",
      thumb: "./assets/proj/sq1/1.png",
      link: "./sq1"
    },
    {
      id: 2,
      name: "RANDONNEE",
      thumb: "./assets/proj/randonnee/1.png",
      link: "./randonnee"
    },
    {
      id: 3,
      name: "Shirou Tsujimura",
      thumb: "./assets/proj/tsujimurashirou/1.png",
      link: "./tsujimurashirou"
    },
    {
      id: 4,
      name: "Sports Club NAS",
      thumb: "./assets/proj/nas/1.png",
      link: "./nas"
    },
    {
      id: 5,
      name: "Sayuri wo Sagase",
      thumb: "./assets/proj/sayuri/1.png",
      link: "./sayuri"
    },
  ];

  const [index, setIndex] = React.useState(0);
  const [barPositionY, setBarPositionY] = React.useState(0);
  const barRef: React.MutableRefObject<HTMLDivElement | null> = React.useRef(null);

  const {state, dispatch} = React.useContext(StateContext);
  const [animationName, setAnimationName] = React.useState("fi");

  const {productId, setProductId, productIdRef} = React.useContext(ProductContext);


  let animationFlag = false;
  const handleChangeId = (current: number, next: number) => {
    const 
      duration = 500,
      delay = 550;

    if(animationFlag === true) return false;
    animationFlag = true;

    setAnimationName((current < next) ? "fot" : "fob");

    window.setTimeout(() => {
      setAnimationName((current < next) ? "fob" : "fot");
      setProductId(next);
    }, duration);

    window.setTimeout(() => {
      setAnimationName("fi");
      animationFlag = false;
      setIndex(next);

      const bar = barRef.current;
      if(bar){
        const positionY = (next) / (lists.length - 1) * bar?.clientHeight;
        setBarPositionY(positionY);
      }
      createCanvasImages();
    }, duration + delay);    
  };

  const handleClickHandlerPrev = () => {
    const currentId = productIdRef.current ? productIdRef.current : productId;
    const nextId = (currentId - 1 < 0) ? lists.length - 1 : currentId - 1;
    handleChangeId(currentId, nextId);
  };

  const handleClickHandlerNext = () => {
    const currentId = productIdRef.current ? productIdRef.current : productId;
    const nextId = (currentId + 1 > lists.length - 1) ? 0 : currentId + 1;
    handleChangeId(currentId, nextId);
  };

  const handleMouseOverPrev = () => {
    dispatch({
      type: "MOUSESTATECHANGE",
      payload: "up"
    });
  };
  
  const handleMouseOverNext = () => {
    dispatch({
      type: "MOUSESTATECHANGE",
      payload: "down"
    });
  };

  const handleMouseOut = () => {
    dispatch({
      type: "INITMOUSESTATE"
    });    
  };

  const handleClickProduct = (ev: any) => {
    setAnimationName("clicked");
    handleLink.handleClickLink(ev);
  };

  const loadImagePromise = async(src: string) => {
    return new Promise((resolve: (value: HTMLImageElement) => void, reject: (reason?: any) => void) => {
      const img = new Image();
      img.onload = () => resolve(img);
      img.onerror = (e) => reject(e);
      img.src = src;
    });
  };

  const productThumbCanvas: React.MutableRefObject<HTMLCanvasElement | null> = React.useRef(null);
  
  const createCanvasImages = async() => {
    const dpr = window.devicePixelRatio || 1;
    const _canvas = productThumbCanvas.current;
    if(!_canvas) return;
    const ctx = _canvas.getContext("2d");
    const rect = _canvas.getBoundingClientRect();
    _canvas.width = rect.width * dpr;
    _canvas.height = rect.height * dpr;
    if(!ctx) return;
    ctx.scale(dpr, dpr);
    const src = _canvas.dataset.src;
    if(!src) return;
    const image = await loadImagePromise(src);
    ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0, _canvas.width / dpr, _canvas.height / dpr);
    ctx.save();
  };

  React.useEffect(() => {
    createCanvasImages();

    // const timer = () => {
    //   console.log(productId)
    //   window.requestAnimationFrame(timer)
    // }

    // window.requestAnimationFrame(timer)
    
    const bar = barRef.current;
    if(bar){
      setIndex(productId);
      const positionY = (productId) / (lists.length - 1) * bar?.clientHeight;
      setBarPositionY(positionY);
    }

    // pc paging
    window.addEventListener("wheel", ev => {
      if(location.pathname !== "/") return;
      if(ev.deltaY > 10) handleClickHandlerNext();
      if(ev.deltaY < -10) handleClickHandlerPrev();
    });

    // paging
    const swipeEventListener = new SwipeEventListener();
    window.addEventListener("touchstart", ev => {
      if(location.pathname !== "/") return;      
      swipeEventListener.touchStart(ev);   
    });
    window.addEventListener("touchmove", ev => {
      if(location.pathname !== "/") return;      
      swipeEventListener.touchMove(ev);      
    });
    window.addEventListener("touchend", ev => {
      if(location.pathname !== "/") return;      
      const directiron = swipeEventListener.touchEnd(ev);
      if(directiron === "left") handleClickHandlerNext();
      if(directiron === "right") handleClickHandlerPrev();
    });
  }, []);

  return(
    <>
      {/* <div className="handler handlerPrev" onClick={handleClickHandlerPrev} onMouseOver={handleMouseOverPrev} onMouseOut={handleMouseOut} /> */}
        <div className="productsList_bar" ref={barRef}>
          <div className="bar_main" style={{transform: `translateY(${barPositionY}px)`}}>
            <span className="index">{index + 1} / {lists.length}</span>
          </div>
        </div>
        <div className="products_outer">
          <ul className="products">
            <li key={lists[productId].id} className={lists[productId].id !== productId ? "hidden" : "" }>
              {/* <span className={animationName} onClick={handleClickLink} onMouseOver={handleMouseOver} onMouseOut={handleMouseOut} data-path={lists[productId].link} > */}
              <span className={animationName}>
                <h1 onClick={handleClickProduct} onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut} data-path={lists[productId].link}>
                  <span className="parent">{lists[productId].name}</span>
                </h1>
                <div className="imageWrapper" onClick={handleClickProduct} onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut} data-path={lists[productId].link}>
                  <canvas className="product-thumb-canvas" data-src={lists[productId].thumb} ref={productThumbCanvas}/>
                  {/* <img src={lists[productId].thumb} alt=""/> */}
                </div>
              </span>
            </li>
          </ul>
        </div>
      {/* <div className="handler handlerNext" onClick={handleClickHandlerNext} onMouseOver={handleMouseOverNext} onMouseOut={handleMouseOut} /> */}
    </>
  );
};

const Products = () => {
  return (
    <>
      <Product />
    </>
  );
};

export default Products;