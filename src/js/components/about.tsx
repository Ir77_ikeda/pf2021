import * as React from "react";
import { Helmet } from "react-helmet";

import { siteSettings, ComponentsGlobalFunc } from "../app";

const aboutComponent = (props: any) => {
  const skillRef: React.MutableRefObject<HTMLUListElement | null> = React.useRef(null);
  const myAddressCanvasRef: React.MutableRefObject<HTMLCanvasElement | null> = React.useRef(null);

  React.useEffect(() => {
    const _canvas = myAddressCanvasRef.current;
    const dpr = window.devicePixelRatio || 1;

    if(!_canvas) return;
    const ctx = _canvas.getContext("2d");
    const rect = _canvas.getBoundingClientRect();
    _canvas.width = rect.width * dpr;
    _canvas.height = rect.height * dpr;

    if(!ctx) return;
    ctx.scale(dpr, dpr);
    ctx.font = "14px sans-serif";
    ctx.fillStyle = "#fff";
    ctx.fillText("t.i.sagit@gmail.com", 0, 16);    
  });

  return (
    <>
      <Helmet>
        <title>ABOUT | {siteSettings.siteName}</title>
        <link rel="canonical" href={`${siteSettings.siteUrl}about`} />
      </Helmet>
      <section className="section contents_in about" id="about">
        {/* <ComponentsGlobalFunc /> */}
        <div className="main_outer about_main">
          <div className="inner">
            <div className="flex">
            <div className="fl">
              <div className="subsec">
                <h1 className="myname">
                  <span className="jp">池田　匠</span>
                  <span className="en">Ikeda Takumi</span>
                </h1>
                <p className="detail">
                  東京でwebデザイナー、フロントエンドエンジニアをしております。<br />
                  新しいもの、新しい技術が大好きです。
                </p>
              </div>

              <div className="subsec">
                <p className="label">History</p>
                <ul className="history">
                  <li>
                    <span className="year">2014</span>
                    <p>
                      太田情報商科専門学校　卒業
                    </p>
                  </li>
                  <li>
                    <span className="year">2014</span>
                    <p>
                      株式会社ディグラに入社
                    </p>
                  </li>
                  <li>
                    <span className="year">2017</span>
                    <p>
                      株式会社ICAに入社
                    </p>
                  </li>
                </ul>
              </div>

              <div className="subsec">
                <p>
                  御用の方はこちらまでお願いいたします。<br />
                  <canvas className="myAddressCanvas" ref={myAddressCanvasRef} width="150" height="30" />
                </p>
              </div>
            </div>
            <div className="fr">
              <p className="label">Skill</p>
              <ul className="skill" ref={skillRef}>
                <li className="bar html bar-90" data-score="90">
                  <span className="lang">HTML/CSS</span><span className="score">90</span>
                </li>
                <li className="bar sass bar-85" data-score="80">
                  <span className="lang">Sass</span><span className="score">80</span>
                </li>
                <li className="bar javascript bar-85" data-score="80">
                  <span className="lang">ES6(JavaScript)</span><span className="score">80</span>
                </li>
                <li className="bar react bar-80" data-score="70">
                  <span className="lang">React</span><span className="score">80</span>
                </li>
                <li className="bar webpack bar-75" data-score="70">
                  <span className="lang">Webpack</span><span className="score">75</span>
                </li>
                <li className="bar wordpress bar-85" data-score="80">
                  <span className="lang">Wordpress</span><span className="score">85</span>
                </li>
              </ul>
            </div>
          </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default aboutComponent;