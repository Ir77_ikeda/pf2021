import * as React from "react";
import * as ReactDOM from "react-dom";
import { Helmet } from "react-helmet";

import { siteSettings } from "../app";

const err_404 = (props: any) => {
  return (
    <>
      <Helmet>
        <title>404 | {siteSettings.siteName}</title>
      </Helmet>
      <section className="section contents_in err_404" id="err_404">
      <p>
        404 Page not found.
      </p>
      </section>
    </>
  );
};

export default err_404;