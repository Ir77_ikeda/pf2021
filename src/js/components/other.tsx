import * as React from "react";
import { useHistory, useLocation } from "react-router-dom";

import { ComponentsGlobalFunc } from "../app";

const aboutComponent = (props: any) => {
  return (
    <section className="section contents_in other" id="other">
      {/* <ComponentsGlobalFunc /> */}
      <div className="main_outer about_main">
        <div className="inner">
          <div className="collection_mv">
            <ul>
              <li/>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default aboutComponent;