import * as React from "react";
import { Helmet } from "react-helmet";

import {siteSettings, ComponentsGlobalFunc, HandleLink} from "../../app";

const productComponent = (props: any) => {
  const galleryRef: React.MutableRefObject<HTMLImageElement[] | HTMLVideoElement[]> = React.useRef([]);

  React.useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if(entry.isIntersecting){
          entry.target.classList.add("active");
          if(entry.target instanceof HTMLVideoElement) entry.target.play();
        }else{
          // entry.target.classList.remove("active");
        }
      });
    });
  
    for(let i = 0, len = galleryRef.current.length; i < len; i++){
      const target = galleryRef.current[i];
      observer.observe(target);
    }
  }, []);

  const bgStyle = {
    backgroundImage: `url(../assets/proj/randonnee/1.png)`,
  };

  const handleLink = new HandleLink();

  return (
    <>
      <Helmet>
        <title>RANDONNEE | {siteSettings.siteName}</title>
        <link rel="canonical" href={`${siteSettings.siteUrl}randonnee`} />
      </Helmet>

      <section className="section contents_in product" id="randonnee">
        {/* <ComponentsGlobalFunc /> */}
        <div className="product_head darkBg" style={bgStyle}>
          <div className="detail">
            <h1 className="productname">
              RANDONNEE
            </h1>
            <p className="release">August 2020</p>
            <a href="https://visitmatsumoto.com/randonnee/" target="_blank" className="launchBtn" onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut}><span>Launch</span></a>
          </div>
        </div>

        <div className="main_outer product_main">
          <div className="inner">
            <div className="detail">
              <p>
                新まつもと物語 <a href="https://visitmatsumoto.com/" target="_blank">(https://visitmatsumoto.com/)</a>  内のキャンペーンページ「ランドネ」のコーディングを担当いたしました。
              </p>
              <ul className="tag">
              <li>
                  HTML/CSS
                </li>
                <li>
                  Javascript
                </li>
                <li>
                   Wordpress
                </li>
              </ul>
            </div>

            <ul className="gallery">
              <li className="image_outer pc">
                <video src="../assets/proj/randonnee/1.mp4" ref={(ref) => {
                  if(ref) galleryRef.current[0] = ref;
                }} loop playsInline />
              </li>
              <li className="image_outer pc">
                <img src="../assets/proj/randonnee/2.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[1] = ref;
                }} />
              </li>
              <li className="image_outer pc">
                <img src="../assets/proj/randonnee/3.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[2] = ref;
                }} />
              </li>
              <li className="image_outer col-2 sp">
                <img src="../assets/proj/randonnee/4.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[3] = ref;
                }} />
              </li>
              <li className="image_outer col-2 sp">
                <img src="../assets/proj/randonnee/5.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[4] = ref;
                }} />
              </li>
              <li className="image_outer col-2 sp">
                <img src="../assets/proj/randonnee/6.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[5] = ref;
                }} />
              </li>
              <li className="image_outer col-2 sp">
                <img src="../assets/proj/randonnee/7.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[6] = ref;
                }} />
              </li>
            </ul>
          </div>
        </div>
      </section>
    </>
  );
};

export default productComponent;