import * as React from "react";
import { Helmet } from "react-helmet";

import {siteSettings, ComponentsGlobalFunc, HandleLink} from "../../app";

const productComponent = (props: any) => {
  const galleryRef: React.MutableRefObject<HTMLImageElement[] | HTMLVideoElement[]> = React.useRef([]);

  React.useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if(entry.isIntersecting){
          entry.target.classList.add("active");
          if(entry.target instanceof HTMLVideoElement) entry.target.play();
        }else{
          // entry.target.classList.remove("active");
        }
      });
    });
  
    for(let i = 0, len = galleryRef.current.length; i < len; i++){
      const target = galleryRef.current[i];
      observer.observe(target);
    }
  }, []);

  const bgStyle = {
    backgroundImage: `url(../assets/proj/sq1/1.png)`,
  };

  const handleLink = new HandleLink();

  return (
    <>
      <Helmet>
        <title>FUJIFILM NEW PRODUCT instax SQUARE SQ1 | {siteSettings.siteName}</title>
        <link rel="canonical" href={`${siteSettings.siteUrl}sq1`} />
      </Helmet>

      <section className="section contents_in product" id="sq1">
        {/* <ComponentsGlobalFunc /> */}
        <div className="product_head darkBg" style={bgStyle}>
          <div className="detail">
            <h1 className="productname">
              <span className="small">FUJIFILM NEW PRODUCT <br /></span>instax SQUARE SQ1
            </h1>
            <p className="release">October 2020</p>
            <a href="https://instax.jp/sq1/" target="_blank" className="launchBtn" onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut}><span>Launch</span></a>
          </div>
        </div>

        <div className="main_outer product_main">
          <div className="inner">
            <div className="detail">
              <p>
                富士フイルム株式会社様より発売された新製品 "instax SQUARE SQ1" のコーディングを担当いたしました。
              </p>
              <ul className="tag">
              <li>
                  HTML/CSS
                </li>
                <li>
                  Javascript
                </li>
                <li>
                  GSAP
                </li>
              </ul>
            </div>

            <ul className="gallery">
              <li className="image_outer pc">
                <img src="../assets/proj/sq1/2.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[0] = ref;
                }} />
              </li>
              <li className="image_outer col-3 sp">
                <img src="../assets/proj/sq1/3.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[1] = ref;
                }} />
              </li>
              <li className="image_outer col-3 sp">
                <img src="../assets/proj/sq1/4.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[2] = ref;
                }} />
              </li>
              <li className="image_outer col-3 sp">
                <img src="../assets/proj/sq1/5.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[3] = ref;
                }} />
              </li>
            </ul>
          </div>
        </div>
      </section>
    </>
  );
};

export default productComponent;