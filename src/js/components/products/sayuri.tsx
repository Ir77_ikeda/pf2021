import * as React from "react";
import { Helmet } from "react-helmet";

import {siteSettings, ComponentsGlobalFunc, HandleLink} from "../../app";

const productComponent = (props: any) => {
  const galleryRef: React.MutableRefObject<HTMLImageElement[] | HTMLVideoElement[]> = React.useRef([]);

  React.useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if(entry.isIntersecting){
          entry.target.classList.add("active");
          if(entry.target instanceof HTMLVideoElement) entry.target.play();
        }else{
          // entry.target.classList.remove("active");
        }
      });
    });
  
    for(let i = 0, len = galleryRef.current.length; i < len; i++){
      const target = galleryRef.current[i];
      observer.observe(target);
    }
  }, []);

  const bgStyle = {
    backgroundImage: `url(../assets/proj/sayuri/1.png)`,
  };

  const handleLink = new HandleLink();

  return (
    <>
      <Helmet>
        <title>Sayuri wo Sagase | {siteSettings.siteName}</title>
        <link rel="canonical" href={`${siteSettings.siteUrl}sayuri`} />
      </Helmet>

      <section className="section contents_in product" id="sayuri">
        {/* <ComponentsGlobalFunc /> */}
        <div className="product_head darkBg" style={bgStyle}>
          <div className="detail">
            <h1 className="productname">
              Sayuri wo Sagase <br />NEW SONG SPECIAL SITE
            </h1>
            <p className="release">2016</p>
            {/* <a href="#" target="_blank" className="launchBtn" onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut}><span>Launch</span></a> */}
          </div>
        </div>

        <div className="main_outer product_main">
          <div className="inner">
            <div className="detail">
              <p>
              株式会社ソニーミュージック様より、アーティスト「酸欠少女　さユり」様の新曲に合わせたキャンペーンページの実装を担当いたしました。
              </p>
              <ul className="tag">
              <li>
                  HTML/CSS
                </li>
                <li>
                  JQuery
                </li>
                <li>
                  Device Orientation   
                </li>
              </ul>
            </div>

            <ul className="gallery">
              <li className="image_outer pc">
                <video src="../assets/proj/sayuri/1.mp4" ref={(ref) => {
                  if(ref) galleryRef.current[0] = ref;
                }} loop playsInline />
              </li>
              <li className="image_outer col-3 sp">
                <img src="../assets/proj/sayuri/2.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[1] = ref;
                }} />
              </li>
              <li className="image_outer col-3 sp">
                <img src="../assets/proj/sayuri/3.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[2] = ref;
                }} />
              </li>
              <li className="image_outer col-3 sp">
                <img src="../assets/proj/sayuri/4.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[3] = ref;
                }} />
              </li>
            </ul>
          </div>
        </div>
      </section>
    </>
  );
};

export default productComponent;