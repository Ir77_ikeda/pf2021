import * as React from "react";
import { Helmet } from "react-helmet";

import {siteSettings, ComponentsGlobalFunc, HandleLink} from "../../app";

const productComponent = (props: any) => {
  const galleryRef: React.MutableRefObject<HTMLImageElement[] | HTMLVideoElement[]> = React.useRef([]);

  React.useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if(entry.isIntersecting){
          entry.target.classList.add("active");
          if(entry.target instanceof HTMLVideoElement) entry.target.play();
        }else{
          // entry.target.classList.remove("active");
        }
      });
    });
  
    for(let i = 0, len = galleryRef.current.length; i < len; i++){
      const target = galleryRef.current[i];
      observer.observe(target);
    }
  }, []);

  const bgStyle = {
    backgroundImage: `url(../assets/proj/tsujimurashirou/1.png)`,
  };

  const handleLink = new HandleLink();

  return (
    <>
      <Helmet>
        <title>Shirou Tsujimura | {siteSettings.siteName}</title>
        <link rel="canonical" href={`${siteSettings.siteUrl}tsujimurashirou`} />
      </Helmet>

      <section className="section contents_in product" id="tsujimurashirou">
        {/* <ComponentsGlobalFunc /> */}
        <div className="product_head darkBg" style={bgStyle}>
          <div className="detail">
            <h1 className="productname">
              Shirou Tsujimura <br />INTRODUCE SITE
            </h1>
            <p className="release">2019</p>
            <a href="https://www.tachikichi.co.jp/webgallery/tsujimura/" target="_blank" className="launchBtn" onMouseOver={handleLink.handleMouseOver} onMouseOut={handleLink.handleMouseOut}><span>Launch</span></a>
          </div>
        </div>

        <div className="main_outer product_main">
          <div className="inner">
            <div className="detail">
              <p>
              株式会社たち吉様より、陶芸家である辻村史郎氏の作品集ページを作成いたしました。
              </p>
              <ul className="tag">
              <li>
                  HTML/CSS
                </li>
                <li>
                  Javascript
                </li>
              </ul>
            </div>

            <ul className="gallery">
              <li className="image_outer pc">
                <img src="../assets/proj/tsujimurashirou/2.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[0] = ref;
                }} />
              </li>
              <li className="image_outer pc">
                <img src="../assets/proj/tsujimurashirou/3.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[1] = ref;
                }} />
              </li>
              <li className="image_outer col-2 sp">
                <img src="../assets/proj/tsujimurashirou/4.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[2] = ref;
                }} />
              </li>
              <li className="image_outer col-2 sp">
                <img src="../assets/proj/tsujimurashirou/5.png" alt="" ref={(ref) => {
                  if(ref) galleryRef.current[3] = ref;
                }} />
              </li>
            </ul>
          </div>
        </div>
      </section>
    </>
  );
};

export default productComponent;