import * as React from "react";
import { Helmet } from "react-helmet";

import { siteSettings, ComponentsGlobalFunc } from "../app";
import Products, {Scroll_guide} from "../components/products";

export const indexComponent = () => {
  return (
    <>
      <Scroll_guide />    
      <Helmet>
        <title>{siteSettings.siteName}</title>
        <link rel="canonical" href={`${siteSettings.siteUrl}`} />
      </Helmet>
      <div className="index_outer">
        <section className="section contents_in" id="index">
          {/* <ComponentsGlobalFunc /> */}
          <Products />
        </section>
      </div>
    </>
    
  );
};

export default indexComponent;